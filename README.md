# go-hashfiles

This action is to compute the SHA256 checksum of specified files.

**NOTE:** This action is written in Go. Please make sure the `runs-on` runner supports running Go actions.

## Usage

``` yml
- uses: actions/go-hashfiles@v0.0.1
  with:
    # The working dir for the action.
    # Default: ${{ github.workspace }}
    workdir: ''

    # The patterns used to match files.
    # Multiple patterns should be seperated with `\n`
    patterns: ''
```

## Output

|Output Item|Description|
|---|---|
|hash|The computed checksum|
|matched-files|The files matched by the patterns|

## Example
``` yml
- uses: actions/go-hashfiles@v0.0.1
  id: get-hash
  with: 
    patterns: |-
      go.sum
      ./admin/*
      **/package-lock.json
- name: Echo hash
  run: echo ${{ steps.get-hash.outputs.hash }}
```


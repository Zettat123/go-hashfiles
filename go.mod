module go-hashfiles

go 1.20

require (
	github.com/go-git/go-git/v5 v5.6.0
	github.com/sethvargo/go-githubactions v1.1.0
)

require (
	github.com/acomagu/bufpipe v1.0.3 // indirect
	github.com/go-git/gcfg v1.5.0 // indirect
	github.com/go-git/go-billy/v5 v5.4.0 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/sethvargo/go-envconfig v0.8.0 // indirect
	golang.org/x/net v0.2.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
